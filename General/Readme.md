# General

1. What happens when you try to send 2 transactions (exactly the same) to 2 different nodes in blockchain network at the same time?

```
In this case, it is called `double-sending` and consensus algorithms have to handle this problem. In case Ethereum, we have a nonce takes care of the double spend problem. If you have 4 ETH and tries to send two transactions of 3 ETH each, only one will get approved (the one having lower nonce). It's simply a sequence and it gets incremented by 1.

```
2. What happens if you try to send 2 transactions that the same input data but different gas price to 2 different nodes in ethereum network?

```
The transaction which has the first nonce will be accepted no matter the gas price is high or low in case they have different nonce.
And to clear the concept there are two nonce in a block. One nonce is present in the header of block and the other is responsible for the transaction sequence.

```

3. What is nonce number does in blockchain network and ethereum network?

```
Nonce standards for `number only used once`, refers to the first number a blockchain miner needs to discover before solving for a block in the blockchain. Each time you send a transaction, the nonce value increases by 1. (quite explain in  #2)

Ethereum uses nonce number to protect against replay attacks.
```
4. Are there any way to override a information of block in blockchain?

```
No, we cannot except you have a supper computer can override all blocks and re-calculate all of things in blockchainThat's impossible!

```

