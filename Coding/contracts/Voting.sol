pragma solidity >=0.4.22 <0.8.0;

contract Voting {

    struct Candidate {
        bytes32 name;
        uint index;
    }
    mapping(address => Candidate) public voters;
    mapping (bytes32 => uint256) public votesReceived;

    uint public candidatesCount;
    uint public expiredTime; 

    bytes32[] public candidateList;
    event votedEvent (
        bytes32 indexed _candidateName
    );

    mapping(bytes32 => address[]) acountsVoted; 

    constructor (bytes32[] memory candidateNames) public {
        expiredTime = block.timestamp + 172800; // 2 days in seconds
        candidateList = candidateNames;
    }

    function voteForCandidate (bytes32 _candidate) public {

        // require a valid candidate
        require(validCandidate(_candidate));

        // check vaild time
        require(block.timestamp <= expiredTime, "Time for voting already ended.");

        // mark that voter has voted
        bytes32 _oldCandidate = voters[msg.sender].name;
        if(_oldCandidate == 0 ) {
            votesReceived[_candidate] ++;
            acountsVoted[_candidate].push(msg.sender);
            voters[msg.sender].index = acountsVoted[_candidate].length;
            voters[msg.sender].name = _candidate;

        } else if (_candidate != _oldCandidate ) {
            voters[msg.sender].name = _candidate;
            votesReceived[_oldCandidate] --;

            delete  acountsVoted[_candidate][voters[msg.sender].index];

            votesReceived[_candidate] ++;
            acountsVoted[_candidate].push(msg.sender);
            voters[msg.sender].index = acountsVoted[_candidate].length;
        }


        emit votedEvent(_candidate);
    }

    function totalVotesFor(bytes32 _candidate) view public returns (uint256) {
        require(validCandidate(_candidate));
        return votesReceived[_candidate];
    }
    function validCandidate(bytes32 _candidate) view public returns (bool) {
        for(uint i = 0; i < candidateList.length; i++) {
            if (candidateList[i] == _candidate) {
                return true;
            }
        }
        return false;
    }

    function getAllVotedAccounts() view public returns (address[] memory trump, address[] memory biden) {
        return(acountsVoted["Donald Trump"], acountsVoted["Joe Biden"]);  
    }
}