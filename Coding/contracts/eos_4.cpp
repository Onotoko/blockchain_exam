/**
 *  @file
 *  @copyright defined in eos/LICENSE.txt
 */
#pragma once
#include <eosio/eosio.hpp>
#include <eosio/time.hpp>
#include <eosio/system.hpp>
#include <eosio/asset.hpp>
#include <eosio/crypto.hpp>
#include <eosio/symbol.hpp>

#include <vector>
#include <string>


namespace eosio
{

using std::string;

class[[eosio::contract("voting")]] voting : public contract
{
    public:
      using contract::contract;
      voting(eosio::name receiver, eosio::name code, eosio::datastream<const char *> ds)
          : eosio::contract(receiver, code, ds) {}
      
      /**
       * Initial candidates
       * 
       * @param candidate_id : The ID of candidate
       * @param candidate_name: The name of candidate
       * 
       * @return void
       */ 

      [[eosio::action]] 
      void addcandidate(uint64_t candidate_id,
                        const std::string& candidate_name)
      {
            require_auth( get_self() );

            votings voting_table(get_self(), get_self().value);
    
            auto iter = voting_table.find(candidate_id);
            
            check(iter == voting_table.end(), "This candidate has been existed!");
    
            voting_table.emplace(get_self(), [&](auto & v) {
                  v.candidate_id        = candidate_id;
                  v.candidate_name      = candidate_name;
                  v.vote_count          = 0;
            });

      }

      /**
       * Voting action
       * 
       * @param candidate_id : The ID of candidate
       * @param voter: Who did a vote for that candidate
       * 
       * @return void
       */ 

      [[eosio::action]] 
      void vote(uint64_t candidate_id,
                const name& voter)
      {
            require_auth( voter );

            check(candidate_id >= 0, "The ID of candidate is a positive number!");

            votings voting_table(get_self(), get_self().value);
            auto viter = voting_table.find(candidate_id);

            check(viter != voting_table.end(), "This candidate has not found!");

            // Check that if the voter voted before
            
            voters voter_table(voter, voter.value);
            auto iter = voter_table.find(voter);

            if(iter == voting_table.end()){
                  voter_table.emplace(voter, [&](auto & v) {
                        v.candidate_id        = candidate_id;
                  });
                  
                  voting_table.modify(iter, voter, [&](auto & v) {
                        v.vote_count ++;
                  });
            } else {
                  
                  voter_table.modify(iter, voter, [&](auto & v) {
                        v.candidate_id = candidate_id;
                  });

                  voting_table.modify(iter, voter, [&](auto & v) {
                        v.vote_count --;
                  });
            }

      }


private:
 
      struct [[eosio::table]] voting
      {
            uint64_t    candidate_id;
            std::string candidate_name;            
            uint64_t    vote_count;

            uint64_t primary_key() const { return candidate_id; }
      };
      struct [[eosio::table]] voters
      {
            name        account_name;
            uint64_t    candidate_id;


            uint64_t primary_key() const { return account_name.value; }
            uint64_t get_voter() const { return candidate_id; }
      };

      typedef eosio::multi_index< "voting"_n, voting> votings;
      
      typedef eosio::multi_index< "voter"_n, indexed_by<"candidate_id"_n, const_mem_fun<voting, uint64_t, &voters::get_voter>>> voters;
};

} // namespace eosio
EOSIO_DISPATCH(eosio::voting, (addcandidate)(vote))