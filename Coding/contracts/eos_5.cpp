/**
 *  @file
 *  @copyright defined in eos/LICENSE.txt
 */
#pragma once
#include <eosio/eosio.hpp>
#include <eosio/time.hpp>
#include <eosio/system.hpp>
#include <eosio/asset.hpp>
#include <eosio/crypto.hpp>
#include <eosio/symbol.hpp>

#include <vector>
#include <string>


namespace eosio
{

using std::string;

class[[eosio::contract("token")]] token : public contract
{
    public:
      using contract::contract;
      token(eosio::name receiver, eosio::name code, eosio::datastream<const char *> ds)
          : eosio::contract(receiver, code, ds) {}


      /**
       * Creating a new token 
       */
      [[eosio::action]]
       void create(const name& issuer,
                   const asset& maximum_supply)
        {
            require_auth(get_self());

            check(issuer.value == _self.value, "The issuer only admin account");

            auto sym = maximum_supply.symbol;
            check(sym.is_valid(), "The invalid symbol name");
            check(maximum_supply.is_valid(), "The invalid supply");
            check(maximum_supply.amount > 0, "The max-supply must be positive");

            stats statstable(_self, sym.code().raw());
            auto existing = statstable.find(sym.code().raw());
            check(existing == statstable.end(), "The token with symbol already exists");

            statstable.emplace(get_self(), [&](auto &s) {
                s.supply.symbol = maximum_supply.symbol;
                s.max_supply = maximum_supply;
                s.issuer = issuer;
            });
        }

      /**
       * Issuesing token with circulating supply to an user 
       */
      [[eosio::action]]
       void issue(
           const name& to, 
           const asset& quantity, 
           const string& memo)
      {
            auto sym = quantity.symbol;
            check(sym.is_valid(), "The invalid symbol name");
            check(memo.size() <= 256, "The memo has more than 256 bytes");

            stats statstable(get_self(), sym.code().raw());
            auto existing = statstable.find(sym.code().raw());
            check(existing != statstable.end(), "The token with symbol does not exist, create token before issue");
            const auto &st = *existing;
            check( to == st.issuer, "Tokens can only be issued to issuer account" );
            
            require_auth(get_self());
            check(quantity.is_valid(), "The invalid quantity");
            check(quantity.amount > 0, "Must issue positive quantity");

            check(quantity.symbol == st.supply.symbol, "The symbol precision mismatch");
            check(quantity.amount <= st.max_supply.amount - st.supply.amount, "The quantity exceeds available supply");

            statstable.modify(st, same_payer, [&](auto &s) {
                s.supply += quantity;
            });

            add_balance(st.issuer, quantity, st.issuer);
      }
      
      /**
       * Transfering token between eos accounts
       */ 

      [[eosio::action]]
       void transfer(const name& from,
                    const name& to,
                    const asset& quantity,
                    const string& memo)
        {
            check( from != to, "Cannot transfer to self" );
            require_auth( from );
            check( is_account( to ), "To account does not exist");

            auto sym = quantity.symbol.code();
            stats statstable( get_self(), sym.raw() );
            const auto& st = statstable.get( sym.raw() );

            require_recipient( from );
            require_recipient( to );

            check( quantity.is_valid(), "invalid quantity" );
            check( quantity.amount > 0, "Must transfer positive quantity" );
            check( quantity.symbol == st.supply.symbol, "Symbol precision mismatch" );
            check( memo.size() <= 256, "Memo has more than 256 bytes" );

            auto payer = has_auth( to ) ? to : from;

            sub_balance( from, quantity );
            add_balance( to, quantity, payer );
        }

        /**
         * Burn token 
         */
        [[eosio::action]]
        void burn(const asset& quantity, const string& memo)
        {
            check(quantity.is_valid(), "Invalid format of quantity");
            check(quantity.amount > 0, "Must positive quantity");
            check( memo.size() <= 256, "Memo has more than 256 bytes" );
            
            auto sym = quantity.symbol;
        
            stats statstable(get_self(), sym.code().raw());
            auto st = statstable.find(sym.code().raw());
            check(st != statstable.end(), "The token with symbol does not exist, we cannot burn it");
           
            require_auth( st.issuer );
            
            statstable.modify(st, same_payer, [&](auto &s) {
                s.max_supply -= quantity;
            });

            sub_balance( st.issuer, quantity );

        }
      
      static asset get_supply( const name& token_contract_account, const symbol_code& sym_code )
      {
            stats statstable( token_contract_account, sym_code.raw() );
            const auto& st = statstable.get( sym_code.raw() );
            return st.supply;
      }

      static asset get_balance( const name& token_contract_account, const name& owner, const symbol_code& sym_code )
      {
            accounts accountstable( token_contract_account, owner.value );
            const auto& ac = accountstable.get( sym_code.raw() );
            return ac.balance;
      }

private:
      struct [[eosio::table]] account {
            asset    balance;

            uint64_t primary_key()const { return balance.symbol.code().raw(); }
      };

      struct [[eosio::table]] currency_stats {
            asset    supply;
            asset    max_supply;
            name     issuer;

            uint64_t primary_key()const { return supply.symbol.code().raw(); }
      };

      typedef eosio::multi_index< "accounts"_n, account > accounts;
      typedef eosio::multi_index< "stat"_n, currency_stats > stats;



      void sub_balance(const name& owner, const asset& value);
      void add_balance(const name& owner, const asset& value, const name& ram_payer);
};

} // namespace eosio
EOSIO_DISPATCH(eosio::token, (create)(issue)(transfer)(burn))