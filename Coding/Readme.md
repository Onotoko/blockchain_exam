# Coding

1. Write a simple dapp (Solidity on Ethereum) that user can using metamask to vote for Trump or Biden with following condition:
  - There are 2 options to choose, voting for Donald Trump or Joe Biden
  - One account can vote only one time.
  - Before specific time (can be configured at deployment), user can change his decision. 
  - After endtime user cannot change his vote, only can view
  - Show total vote, how many vote for Trump and how many vote for Joe Biden
  - Show list accounts vote for Trump, and list account vote for Joe Biden

2. Write an service that listen to dapp, and alert to console when there is any changed in voting result.

3. Provide script to deploy contracts on testnet (ropsten or rinkeby)
4. Provide some sample code for EOS smart contracts for review
5. Write a smart contract which can accept old token and after burning it issue the new token to that account.